require "rack/test"
require "webrat"
require "minitest/autorun"

require_relative "server"
require_relative "lib/kgb_config"

Webrat.configure do |config|
	config.mode = :rack
end

class AppTest < Minitest::Test
	include Rack::Test::Methods
	include Webrat::Methods
	include Webrat::Matchers

	def setup
		Webhook.new
	end


end

class KGBConfigTest < Minitest::Test
	def setup
		@cfg = KGBConfig.new(['config.d'])
	end

	def test_get_channel()
		assert_equal(@cfg.get_channel('my-repository'), {"password"=>"verysecret"})
		assert_nil(@cfg.get_channel('nonexistant'))
	end

	def test_verify_channel()
		assert_equal(@cfg.verify_password('my-repository', 'wrongpassword'), false)
		assert_equal(@cfg.verify_password('my-repository', 'verysecret'), true)
	end

end
