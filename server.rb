require 'sinatra/base'
require "sinatra/config_file"

require 'json'
require_relative 'lib/kgb_bridge'

class Webhook < Sinatra::Base
  register Sinatra::ConfigFile

  config_file 'config.yml'

  unless settings.respond_to?('kgb_remote')
    raise "kgb_remote setting missing in config.yml"
  end


  kgb_bridge = KGBBridge.new(kgb_remote: settings.kgb_remote)

  post '/kgbbridge/:repository' do
    if ! request.env['HTTP_X_GITLAB_TOKEN']
      status 401
      return 'Wrong password or token missing'
    elsif ! kgb_bridge.kgb_config.get_channel(params['repository'])
      status 404
      return 'Channel not found'
    elsif kgb_bridge.kgb_config.verify_password(params['repository'],request.env['HTTP_X_GITLAB_TOKEN'] ) == false
      status 401
      return 'Wrong password'
    end

    request.body.rewind
    request_payload = JSON.parse(request.body.read)

    # lets identify the kind of the request_payload
    request_type = request_payload['object_kind']

    if kgb_bridge.respond_to?("handle_#{request_type}")
      rc = kgb_bridge.send("handle_#{request_type}", params['repository'], request_payload)
      status rc['status']
      rc['message']
    else
      status 500
      "No handler found for #{request_type} messages"
    end
  end
  run! if __FILE__ == $0
end
