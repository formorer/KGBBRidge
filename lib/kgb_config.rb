require 'yaml'
require 'find'
require 'pp'

class KGBConfig
  def initialize(configfiles=[])
    @configfiles = configfiles
    @config = Hash.new
    read_files(configfiles)
  end

  def read_files(configs)
    configs.each { |element|
      if Dir.exists?(element)
        Find.find(element) do |f|
          read_files([f]) if File.file?(f) and f =~ /.*\.ya?ml$/
        end
      else
        load_file(element)
      end
    }
  end

  def load_file(file)
    merge_recursively(@config, YAML.load_file(file))
  end

  def config
    @config
  end

  def verify_password(channel, password)
    if config['repositories'][channel]
      return true if config['repositories'][channel]['password'] == password
      return false
    else
      return nil
    end
  end

  def get_password(channel)
    channel_config = get_channel(channel)

    return nil if ! channel_config
    return config['repositories'][channel]['password'] if config['repositories'][channel]['password']
    return nil
  end

  def get_channel(channel)
    return config['repositories'][channel] if config['repositories'][channel]
    return nil
  end

  private


  def merge_recursively(a, b)
    a.merge!(b) {|key, a_item, b_item| merge_recursively(a_item, b_item) }
  end

end
