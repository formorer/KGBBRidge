require_relative 'kgb_config'
require 'shorturl'
require 'KGBClient'

class KGBBridge
  attr_accessor :kgb_config

  def initialize(kgb_config: ['config.d'], kgb_remote: nil)
    raise 'kgb_remote missing' unless kgb_remote
    @kgb_config = KGBConfig.new(kgb_config)
    @kgb_client = KGBClient.new(kgb_remote)
  end

  def handle_push(repository, payload)
    payload['commits'].each do |commit|
      commit_message = commit['message'].gsub(/\n/," ")
      short_url = ShortURL.shorten(commit['url'], :tinyurl)
      send_message repository, "[#{payload['repository']['name']}] #{commit['author']['name']} | New Commit: #{commit_message}"
      send_message repository, "View Commit: #{short_url}"
    end
    { 'rc' => 200, 'message' => 'message sent'}
  end

  def handle_tag_push(repository, payload)
    send_message repository, "[#{payload['repository']['name']}] #{payload['user_name']} | Pushed tag: #{payload['ref']}"
    { 'rc' => 200, 'message' => 'message sent'}
  end

  def handle_note(repository, payload)
    short_url = ShortURL.shorten(payload['object_attributes']['url'], :tinyurl)
    send_message repository, "[#{payload['user']['name']}] #{payload['user_name']} | Added a note to issue ##{payload['issue']['iid']}"
    send_message repository, "URL: #{short_url}"
    { 'rc' => 200, 'message' => 'message sent'}
  end
  private

  def send_message(channel, message)
    puts "Send #{message} to #{channel}"
    @kgb_client.relay_message(message: message, project: channel, token: @kgb_config.get_password(channel))
  end

  def safe_unknown_request(payload)
    json = JSON.pretty_generate(payload)
    File.open(secure_filename(), 'w') { |file| file.write(json << "\n") }
  end

  def secure_filename
    "logs/#{SecureRandom.urlsafe_base64}.request"
  end

end
